# Translation of docs_krita_org_user_manual___gamut_masks.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_user_manual___gamut_masks\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-06 03:34+0200\n"
"PO-Revision-Date: 2019-05-06 17:43+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../user_manual/gamut_masks.rst:1
msgid "Basics of using gamut masks in Krita."
msgstr "Основи використання масок діапазону кольорів у Krita."

#: ../../user_manual/gamut_masks.rst:10 ../../user_manual/gamut_masks.rst:15
msgid "Gamut Masks"
msgstr "Маски діапазону кольорів"

#: ../../user_manual/gamut_masks.rst:19
msgid ""
"Gamut masking is an approach to color formalized by James Gurney, based on "
"the idea that any color scheme can be expressed as shapes cut out from the "
"color wheel."
msgstr ""
"Маски діапазонів кольорів — підхід до побудови системи кольорів, який було "
"формалізовано Джеймсом Гурні (James Gurney). Цей підхід засновано на ідеї "
"про те, що будь-яку схему кольорів можна виразити через форми, вирізані з "
"кола кольорів."

#: ../../user_manual/gamut_masks.rst:21
msgid ""
"It originates in the world of traditional painting, as a form of planning "
"and premixing palettes. However, it translates well into digital art, "
"enabling you to explore and analyze color, plan color schemes and guide your "
"color choices."
msgstr ""
"Цей підхід походить з царини традиційного малювання і є формою планування та "
"попереднього змішування палітр. Втім, він добре масштабується на цифрові "
"художні твори, надаючи вам змогу вивчати та аналізувати колір, планувати "
"схеми кольорів та допомагає у виборі кольорів."

#: ../../user_manual/gamut_masks.rst:23
msgid "How does it work?"
msgstr "Як усе це працює?"

#: ../../user_manual/gamut_masks.rst:25
msgid ""
"You draw one or multiple shapes on top of the color wheel. You limit your "
"color choices to colors inside the shapes. By leaving colors out, you "
"establish a relationship between the colors, thus creating harmony."
msgstr ""
"Ви малюєте одну або декілька форм на колі кольорів. Ви обмежуєте ваш вибір "
"кольорів кольорами всередині форм. Виключаючи певні кольори, ви встановлюєте "
"зв'язок між кольорами, а отже, створюєте гармонію."

#: ../../user_manual/gamut_masks.rst:28
msgid ""
"Gamut masking is available in both the Advanced and Artistic Color Selectors."
msgstr ""
"Маскування діапазонів кольорів доступне як на розширеному, так і на "
"художньому засобі вибору кольорів."

#: ../../user_manual/gamut_masks.rst:33
msgid ""
"`Color Wheel Masking, Part 1 by James Gurney <https://gurneyjourney.blogspot."
"com/2008/01/color-wheel-masking-part-1.html>`_"
msgstr ""
"`Маскування за кольоровим колом, частина 1, Джеймс Ґурні (James Gurney) "
"<https://gurneyjourney.blogspot.com/2008/01/color-wheel-masking-part-1."
"html>`_"

#: ../../user_manual/gamut_masks.rst:34
msgid ""
"`The Shapes of Color Schemes by James Gurney <https://gurneyjourney.blogspot."
"com/2008/02/shapes-of-color-schemes.html>`_"
msgstr ""
"`Форми схем кольорів, Джеймс Ґурні (James Gurney) <https://gurneyjourney."
"blogspot.com/2008/02/shapes-of-color-schemes.html>`_"

#: ../../user_manual/gamut_masks.rst:35
msgid ""
"`Gamut Masking Demonstration by James Gourney (YouTube) <https://youtu.be/"
"qfE4E5goEIc>`_"
msgstr ""
"`Демонстрація маскування за діапазоном кольорів, яку створено Джеймсом Гурні "
"(James Gourney, YouTube) <https://youtu.be/qfE4E5goEIc>`_"

#: ../../user_manual/gamut_masks.rst:39
msgid "Selecting a gamut mask"
msgstr "Вибір маски діапазону кольорів"

#: ../../user_manual/gamut_masks.rst:41
msgid ""
"For selecting and managing gamut masks open the :ref:`gamut_mask_docker`:  :"
"menuselection:`Settings --> Dockers --> Gamut Masks`."
msgstr ""
"Щоб вибрати маски діапазонів кольорів та керувати ними, відкрийте :ref:"
"`бічну панель масок діапазонів кольорів <gamut_mask_docker>`: :menuselection:"
"`Параметри --> Бічні панелі --> Маски діапазонів кольорів`."

#: ../../user_manual/gamut_masks.rst:45
msgid ".. image:: images/dockers/Krita_Gamut_Mask_Docker.png"
msgstr ".. image:: images/dockers/Krita_Gamut_Mask_Docker.png"

#: ../../user_manual/gamut_masks.rst:45
msgid "Gamut Masks docker"
msgstr "Бічна панель масок діапазону кольорів"

#: ../../user_manual/gamut_masks.rst:47
msgid ""
"In this docker you can choose from several classic gamut masks, like the "
"‘Atmospheric Triad’, ‘Complementary’, or ‘Dominant Hue With Accent’. You can "
"also duplicate those masks and make changes to them (3,4), or create new "
"masks from scratch (2)."
msgstr ""
"За допомогою цієї бічної панелі ви можете вибрати одну з декількох класичних "
"масок діапазонів кольорів, зокрема ‘Atmospheric Triad’, ‘Complementary’ або "
"‘Dominant Hue With Accent’. Ви також можете створити дублікати цих масок і "
"внести до них зміни (3, 4) або створити нові маски з нуля (2)."

#: ../../user_manual/gamut_masks.rst:49
msgid ""
"Clicking the thumbnail icon (1) of the mask applies it to the color "
"selectors."
msgstr ""
"Натискання піктограми-мініатюри маски (1) призводить до застосування маски "
"до засобів вибору кольору."

#: ../../user_manual/gamut_masks.rst:53
msgid ":ref:`gamut_mask_docker`"
msgstr ":ref:`gamut_mask_docker`"

#: ../../user_manual/gamut_masks.rst:57
msgid "In the color selector"
msgstr "У засобі вибору кольору"

#: ../../user_manual/gamut_masks.rst:59
msgid ""
"You can rotate an existing mask directly in the color selector, by dragging "
"the rotation slider on top of the selector (2)."
msgstr ""
"Ви можете обертати наявну маску безпосередньо на панелі засобу вибору "
"кольору перетягуванням повзунка обертання, розташованого над засобом вибору "
"кольору (2)."

#: ../../user_manual/gamut_masks.rst:61
msgid ""
"The mask can be toggled off and on again by the toggle mask button in the "
"top left corner (1)."
msgstr ""
"Маску можна вмикати і вимикати за допомогою кнопки перемикання маски, "
"розташованої у верхньому лівому куті (1)."

#: ../../user_manual/gamut_masks.rst:65
msgid ".. image:: images/dockers/GamutMasks_Selectors.png"
msgstr ".. image:: images/dockers/GamutMasks_Selectors.png"

#: ../../user_manual/gamut_masks.rst:65
msgid "Advanced and Artistic color selectors with a gamut mask"
msgstr ""
"Засоби розширеного та художнього вибору кольору із маскою діапазону кольорів"

#: ../../user_manual/gamut_masks.rst:69
msgid ":ref:`artistic_color_selector_docker`"
msgstr ":ref:`artistic_color_selector_docker`"

#: ../../user_manual/gamut_masks.rst:70
msgid ":ref:`advanced_color_selector_docker`"
msgstr ":ref:`advanced_color_selector_docker`"

#: ../../user_manual/gamut_masks.rst:74
msgid "Editing/creating a custom gamut mask"
msgstr "Редагування і створення нетипової маски діапазону кольорів"

#: ../../user_manual/gamut_masks.rst:78
msgid ""
"To rotate a mask around the center point use the rotation slider in the "
"color selector."
msgstr ""
"Для обертання маски навколо центральної точки скористайтеся повзунком "
"обертання на панелі засобу вибору кольору."

#: ../../user_manual/gamut_masks.rst:80
msgid ""
"If you choose to create a new mask, edit, or duplicate selected mask, the "
"mask template documents open as a new view (1)."
msgstr ""
"Якщо ви накажете програмі створити маску, редагувати або дублювати позначену "
"маску, документи шаблонів масок буде відкрито на новій панелі перегляду (1)."

#: ../../user_manual/gamut_masks.rst:82
msgid ""
"There you can create new shapes and modify the mask with standard vector "
"tools (:ref:`vector_graphics`). Please note, that the mask is intended to be "
"composed of basic vector shapes. Although interesting results might arise "
"from using advanced vector drawing techniques, not all features are "
"guaranteed to work properly (e.g. grouping, vector text, etc.)."
msgstr ""
"Тут ви можете створити нові форми і внести зміни до маски за допомогою "
"стандартних інструментів векторної графіки (:ref:`vector_graphics`). Будь "
"ласка, зауважте, що маска має складатися з базових векторних форм. Хоча "
"цікаві результати можна отримати використанням розширених методик малювання "
"векторних форм, розробники програми не гарантують можливості належним чином "
"скористатися усіма варіантами векторних форм (наприклад групуванням, "
"векторним текстом тощо)."

#: ../../user_manual/gamut_masks.rst:86
msgid ""
"Transformations done through the transform tool or layer transform cannot be "
"saved in a gamut mask. The thumbnail image reflects the changes, but the "
"individual mask shapes do not."
msgstr ""
"Перетворення, які виконано за допомогою інструмента перетворення, або "
"перетворення шару не зберігаються у масці діапазону кольорів. Зображення-"
"мініатюра показує внесені зміни, але у окремих формах маски вони не "
"відтворюються."

#: ../../user_manual/gamut_masks.rst:88
msgid ""
"You can :guilabel:`Preview` the mask in the color selector (4). If you are "
"satisfied with your changes, :guilabel:`Save` the mask (5). :guilabel:"
"`Cancel` (3) will close the editing view without saving your changes."
msgstr ""
"Ви можете здійснювати попередній :guilabel:`Перегляд` маски на панелі вибору "
"кольору (4). Якщо ви задоволені внесеними змінами, натисніть кнопку :"
"guilabel:`Зберегти`, щоб зберегти маску (5). Якщо натиснути кнопку :guilabel:"
"`Скасувати` (3), панель редагування буде закрито без збереження внесених "
"змін."

#: ../../user_manual/gamut_masks.rst:92
msgid ".. image:: images/dockers/Krita_Gamut_Mask_Docker_2.png"
msgstr ".. image:: images/dockers/Krita_Gamut_Mask_Docker_2.png"

#: ../../user_manual/gamut_masks.rst:92
msgid "Editing a gamut mask"
msgstr "Редагування маски діапазону кольорів"

#: ../../user_manual/gamut_masks.rst:96
msgid "Importing and exporting"
msgstr "Імпортування та експортування"

#: ../../user_manual/gamut_masks.rst:98
msgid ""
"Gamut masks can be imported and exported in bundles in the Resource Manager. "
"See :ref:`resource_management` for more information."
msgstr ""
"Маски діапазонів кольорів може бути імпортовано та експортовано у комплектах "
"за допомогою засобу керування ресурсами. Докладніше про це у розділі :ref:"
"`resource_management`."
