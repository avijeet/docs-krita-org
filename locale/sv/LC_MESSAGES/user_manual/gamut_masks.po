# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-06 03:34+0200\n"
"PO-Revision-Date: 2019-07-03 18:25+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../user_manual/gamut_masks.rst:1
msgid "Basics of using gamut masks in Krita."
msgstr "Grunderna i att använda färgomfångsmasker i Krita."

#: ../../user_manual/gamut_masks.rst:10 ../../user_manual/gamut_masks.rst:15
msgid "Gamut Masks"
msgstr "Färgomfångsmasker"

#: ../../user_manual/gamut_masks.rst:19
msgid ""
"Gamut masking is an approach to color formalized by James Gurney, based on "
"the idea that any color scheme can be expressed as shapes cut out from the "
"color wheel."
msgstr ""
"Färgomfångsmaskering är ett tillvägagångssätt för färg formaliserat av James "
"Gurney, baserat på idén att alla färgscheman kan uttryckas som former "
"utklippta från färghjulet."

#: ../../user_manual/gamut_masks.rst:21
msgid ""
"It originates in the world of traditional painting, as a form of planning "
"and premixing palettes. However, it translates well into digital art, "
"enabling you to explore and analyze color, plan color schemes and guide your "
"color choices."
msgstr ""
"Det har sitt ursprung i den traditionella målarvärlden, som en sorts "
"planerings- och förblandningspaletter. Dock översätts det bra till "
"digitalkonst, och gör det möjligt att utforska och analysera färg, planera "
"färgscheman och vägleda färgval."

#: ../../user_manual/gamut_masks.rst:23
msgid "How does it work?"
msgstr "Hur fungerar det?"

#: ../../user_manual/gamut_masks.rst:25
msgid ""
"You draw one or multiple shapes on top of the color wheel. You limit your "
"color choices to colors inside the shapes. By leaving colors out, you "
"establish a relationship between the colors, thus creating harmony."
msgstr ""
"Man ritar en eller flera former ovanpå färghjulet. Man begränsar sitt "
"färgval till färger inne i formerna. Genom att utelämna färger, etableras "
"ett förhållande mellan färgerna, och på så sätt skapas harmoni."

#: ../../user_manual/gamut_masks.rst:28
msgid ""
"Gamut masking is available in both the Advanced and Artistic Color Selectors."
msgstr ""
"Färgomfångsmaskering är tillgänglig i både den avancerade och konstnärliga "
"färgväljaren."

#: ../../user_manual/gamut_masks.rst:33
msgid ""
"`Color Wheel Masking, Part 1 by James Gurney <https://gurneyjourney.blogspot."
"com/2008/01/color-wheel-masking-part-1.html>`_"
msgstr ""
"`Color Wheel Masking, Part 1 by James Gurney <https://gurneyjourney.blogspot."
"com/2008/01/color-wheel-masking-part-1.html>`_"

#: ../../user_manual/gamut_masks.rst:34
msgid ""
"`The Shapes of Color Schemes by James Gurney <https://gurneyjourney.blogspot."
"com/2008/02/shapes-of-color-schemes.html>`_"
msgstr ""
"`The Shapes of Color Schemes by James Gurney <https://gurneyjourney.blogspot."
"com/2008/02/shapes-of-color-schemes.html>`_"

#: ../../user_manual/gamut_masks.rst:35
msgid ""
"`Gamut Masking Demonstration by James Gourney (YouTube) <https://youtu.be/"
"qfE4E5goEIc>`_"
msgstr ""
"`Demonstration av färgomfångsmaskering av James Gourney (YouTube) <https://"
"youtu.be/qfE4E5goEIc>`_"

#: ../../user_manual/gamut_masks.rst:39
msgid "Selecting a gamut mask"
msgstr "Välja en färgomfångsmask"

#: ../../user_manual/gamut_masks.rst:41
msgid ""
"For selecting and managing gamut masks open the :ref:`gamut_mask_docker`:  :"
"menuselection:`Settings --> Dockers --> Gamut Masks`."
msgstr ""
"För att välja och hantera färgomfångsmasker, öppna :ref:"
"`gamut_mask_docker`:  :menuselection:`Inställningar --> Paneler --> "
"Färgomfångsmasker`."

#: ../../user_manual/gamut_masks.rst:45
msgid ".. image:: images/dockers/Krita_Gamut_Mask_Docker.png"
msgstr ".. image:: images/dockers/Krita_Gamut_Mask_Docker.png"

#: ../../user_manual/gamut_masks.rst:45
msgid "Gamut Masks docker"
msgstr "Panelen färgomfångsmasker"

#: ../../user_manual/gamut_masks.rst:47
msgid ""
"In this docker you can choose from several classic gamut masks, like the "
"‘Atmospheric Triad’, ‘Complementary’, or ‘Dominant Hue With Accent’. You can "
"also duplicate those masks and make changes to them (3,4), or create new "
"masks from scratch (2)."
msgstr ""
"Man kan välja bland flera klassiska färgomfångsmasker, såsom ‘Atmospheric "
"Triad’, ‘Complementary’ eller ‘Dominant Hue With Accent’. Man kan också "
"duplicera maskerna och gör ändringar av dem (3, 4), eller skapa nya masker "
"från början (2)."

#: ../../user_manual/gamut_masks.rst:49
msgid ""
"Clicking the thumbnail icon (1) of the mask applies it to the color "
"selectors."
msgstr ""
"Att klicka på maskens miniatyrbildsikon (1) tillämpar den i färgväljarna."

#: ../../user_manual/gamut_masks.rst:53
msgid ":ref:`gamut_mask_docker`"
msgstr ":ref:`gamut_mask_docker`"

#: ../../user_manual/gamut_masks.rst:57
msgid "In the color selector"
msgstr "I färgväljaren"

#: ../../user_manual/gamut_masks.rst:59
msgid ""
"You can rotate an existing mask directly in the color selector, by dragging "
"the rotation slider on top of the selector (2)."
msgstr ""
"Man kan rotera en befintlig mask direkt i färgväljaren genom att dra "
"rotationsreglaget ovanför väljaren (2)."

#: ../../user_manual/gamut_masks.rst:61
msgid ""
"The mask can be toggled off and on again by the toggle mask button in the "
"top left corner (1)."
msgstr ""
"Maskerna kan stängas av och sättas på igen med omkopplarknappen i övre "
"vänstra hörnet (1)."

#: ../../user_manual/gamut_masks.rst:65
msgid ".. image:: images/dockers/GamutMasks_Selectors.png"
msgstr ".. image:: images/dockers/GamutMasks_Selectors.png"

#: ../../user_manual/gamut_masks.rst:65
msgid "Advanced and Artistic color selectors with a gamut mask"
msgstr "Avancerade och konstnärliga färgväljarna med en färgomfångsmask"

#: ../../user_manual/gamut_masks.rst:69
msgid ":ref:`artistic_color_selector_docker`"
msgstr ":ref:`artistic_color_selector_docker`"

#: ../../user_manual/gamut_masks.rst:70
msgid ":ref:`advanced_color_selector_docker`"
msgstr ":ref:`advanced_color_selector_docker`"

#: ../../user_manual/gamut_masks.rst:74
msgid "Editing/creating a custom gamut mask"
msgstr "Redigera eller skapa en egen färgomfångsmask"

#: ../../user_manual/gamut_masks.rst:78
msgid ""
"To rotate a mask around the center point use the rotation slider in the "
"color selector."
msgstr ""
"Använd rotationsreglaget i färgväljaren för att rotera en mask omkring "
"centrumpunkten."

#: ../../user_manual/gamut_masks.rst:80
msgid ""
"If you choose to create a new mask, edit, or duplicate selected mask, the "
"mask template documents open as a new view (1)."
msgstr ""
"Om man har valt att skapa en ny mask, redigera eller duplicera markerad "
"mask, öppnas maskens dokumentmall som en ny vy (1)."

#: ../../user_manual/gamut_masks.rst:82
msgid ""
"There you can create new shapes and modify the mask with standard vector "
"tools (:ref:`vector_graphics`). Please note, that the mask is intended to be "
"composed of basic vector shapes. Although interesting results might arise "
"from using advanced vector drawing techniques, not all features are "
"guaranteed to work properly (e.g. grouping, vector text, etc.)."
msgstr ""
"Där kan man skapa nya former och ändra masken med vanliga vektorverktyg (:"
"ref:`vector_graphics`). Observera att masken är avsedd att sammanfogas av "
"grundläggande vektorformer. Även om intressanta resultat kan uppstå genom "
"att använda avancerade tekniker för att rita vektorer, garanteras inte att "
"alla funktioner fungerar som de ska (t.ex. gruppering, vektortext, etc.)."

#: ../../user_manual/gamut_masks.rst:86
msgid ""
"Transformations done through the transform tool or layer transform cannot be "
"saved in a gamut mask. The thumbnail image reflects the changes, but the "
"individual mask shapes do not."
msgstr ""
"Transformeringar som görs via transformeringsverktyget eller en "
"lagertransformering kan inte sparas i en färgomfångsmask. Miniatyrbilden "
"reflekterar ändringarna, men de individuella maskformerna gör det inte."

#: ../../user_manual/gamut_masks.rst:88
msgid ""
"You can :guilabel:`Preview` the mask in the color selector (4). If you are "
"satisfied with your changes, :guilabel:`Save` the mask (5). :guilabel:"
"`Cancel` (3) will close the editing view without saving your changes."
msgstr ""
"Man kan :guilabel:`Förhandsgranska` masken i färgväljaren (4). När man är "
"nöjd med ändringarna kan man  :guilabel:`Spara` masken (5). :guilabel:"
"`Avbryt` (3) stänger redigeringsvyn utan att spara ändringarna."

#: ../../user_manual/gamut_masks.rst:92
msgid ".. image:: images/dockers/Krita_Gamut_Mask_Docker_2.png"
msgstr ".. image:: images/dockers/Krita_Gamut_Mask_Docker_2.png"

#: ../../user_manual/gamut_masks.rst:92
msgid "Editing a gamut mask"
msgstr "Redigera en färgomfångsmask"

#: ../../user_manual/gamut_masks.rst:96
msgid "Importing and exporting"
msgstr "Importera och exportera"

#: ../../user_manual/gamut_masks.rst:98
msgid ""
"Gamut masks can be imported and exported in bundles in the Resource Manager. "
"See :ref:`resource_management` for more information."
msgstr ""
"Färgomfångsmasker kan importeras och exporteras i packar från "
"resurshanteraren. Se :ref:`resource_management` för mer information."
