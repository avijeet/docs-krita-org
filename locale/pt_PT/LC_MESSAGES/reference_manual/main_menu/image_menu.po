# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-03 14:23+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Krita ref waveletdecompose kbd imagesplit\n"

#: ../../<generated>:1
msgid "Separate Image"
msgstr "Separar a Imagem"

#: ../../reference_manual/main_menu/image_menu.rst:1
msgid "The image menu in Krita."
msgstr "O menu de imagens no Krita."

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Image"
msgstr "Imagem"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Canvas Projection Color"
msgstr "Cor da Projecção da Área de Desenho"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Trim"
msgstr "Recorte"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Resize"
msgstr "Dimensionar"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Scale"
msgstr "Escala"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Mirror"
msgstr "Espelho"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Transform"
msgstr "Transformar"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Convert Color Space"
msgstr "Converter o Espaço de Cores"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Offset"
msgstr "Deslocamento"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Split Channels"
msgstr "Dividir os Canais"

#: ../../reference_manual/main_menu/image_menu.rst:16
msgid "Image Menu"
msgstr "O Menu Imagem"

#: ../../reference_manual/main_menu/image_menu.rst:18
msgid "Properties"
msgstr "Propriedades"

#: ../../reference_manual/main_menu/image_menu.rst:19
msgid "Gives you the image properties."
msgstr "Dá-lhe algumas propriedades da imagem."

#: ../../reference_manual/main_menu/image_menu.rst:20
msgid "Image Background Color and Transparency"
msgstr "Cor de Fundo e Transparência da Imagem"

#: ../../reference_manual/main_menu/image_menu.rst:21
msgid "Change the background canvas color."
msgstr "Mudar a cor de fundo da área de desenho."

#: ../../reference_manual/main_menu/image_menu.rst:22
msgid "Convert Current Image Color Space."
msgstr "Converter o Espaço de Cores Actual da Imagem."

#: ../../reference_manual/main_menu/image_menu.rst:23
msgid "Converts the current image to a new colorspace."
msgstr "Converte a imagem actual para um novo espaço de cores."

#: ../../reference_manual/main_menu/image_menu.rst:24
msgid "Trim to image size"
msgstr "Recortar ao tamanho da imagem"

#: ../../reference_manual/main_menu/image_menu.rst:25
msgid ""
"Trims all layers to the image size. Useful for reducing filesize at the loss "
"of information."
msgstr ""
"Recorta todas as camadas ao tamanho da imagem. É útil para reduzir o tamanho "
"do ficheiro em detrimento da perda de informações."

#: ../../reference_manual/main_menu/image_menu.rst:26
msgid "Trim to Current Layer"
msgstr "Recortar à Camada Actual"

#: ../../reference_manual/main_menu/image_menu.rst:27
msgid ""
"A lazy cropping function. Krita will use the size of the current layer to "
"determine where to crop."
msgstr ""
"Uma função de recorte a pedido. O Krita irá usar o tamanho da camada actual "
"para determinar onde cortar."

#: ../../reference_manual/main_menu/image_menu.rst:28
msgid "Trim to Selection"
msgstr "Recortar à Selecção"

#: ../../reference_manual/main_menu/image_menu.rst:29
msgid ""
"A lazy cropping function. Krita will crop the canvas to the selected area."
msgstr ""
"Uma função de recorte a pedido. O Krita irá recortar a área de desenho à "
"área seleccionada."

#: ../../reference_manual/main_menu/image_menu.rst:30
msgid "Rotate Image"
msgstr "Rodar a Imagem"

#: ../../reference_manual/main_menu/image_menu.rst:31
msgid "Rotate the image"
msgstr "Roda a imagem"

#: ../../reference_manual/main_menu/image_menu.rst:32
msgid "Shear Image"
msgstr "Inclinar Imagem"

#: ../../reference_manual/main_menu/image_menu.rst:33
msgid "Shear the image"
msgstr "Inclina a imagem"

#: ../../reference_manual/main_menu/image_menu.rst:34
msgid "Mirror Image Horizontally"
msgstr "Espelhar a Imagem na Horizontal"

#: ../../reference_manual/main_menu/image_menu.rst:35
msgid "Mirror the image on the horizontal axis."
msgstr "Cria um espelho da imagem no eixo horizontal."

#: ../../reference_manual/main_menu/image_menu.rst:36
msgid "Mirror Image Vertically"
msgstr "Espelhar a Imagem na Vertical"

#: ../../reference_manual/main_menu/image_menu.rst:37
msgid "Mirror the image on the vertical axis."
msgstr "Cria um espelho da imagem no eixo vertical."

#: ../../reference_manual/main_menu/image_menu.rst:38
msgid "Scale to New Size"
msgstr "Escala ao Novo Tamanho"

#: ../../reference_manual/main_menu/image_menu.rst:39
msgid ""
"The resize function in any other program with the :kbd:`Ctrl + Alt + I` "
"shortcut."
msgstr ""
"A função de ajuste do tamanho em qualquer outro programa :kbd:`Ctrl + Alt + "
"I`."

#: ../../reference_manual/main_menu/image_menu.rst:40
msgid "Offset Image"
msgstr "Deslocamento da Imagem"

#: ../../reference_manual/main_menu/image_menu.rst:41
msgid "Offset all layers."
msgstr "Provoca um deslocamento de todas as camadas."

#: ../../reference_manual/main_menu/image_menu.rst:42
msgid "Resize Canvas"
msgstr "Dimensionar a Área de Desenho"

#: ../../reference_manual/main_menu/image_menu.rst:43
msgid "Change the canvas size. Don't confuse this with Scale to new size."
msgstr ""
"Muda o tamanho da área de desenho. Não confundir com a opção 'Escala ao Novo "
"Tamanho'."

#: ../../reference_manual/main_menu/image_menu.rst:44
msgid "Image Split"
msgstr "Divisão da Imagem"

#: ../../reference_manual/main_menu/image_menu.rst:45
msgid "Calls up the :ref:`image_split` dialog."
msgstr "Invoca a janela de :ref:`image_split`."

#: ../../reference_manual/main_menu/image_menu.rst:46
msgid "Wavelet Decompose"
msgstr "Decomposição de Ondas"

#: ../../reference_manual/main_menu/image_menu.rst:47
msgid "Does :ref:`wavelet_decompose` on the current layer."
msgstr "Efectua uma :ref:`wavelet_decompose` sobre a camada actual."

#: ../../reference_manual/main_menu/image_menu.rst:49
msgid ":ref:`Separates <separate_image>` the image into channels."
msgstr ":ref:`Separa <separate_image>` a imagem por canais."
